pima-indians-diabetes-multi
==============================

Multi-ouput regressor based on the Pima Indians diabetes dataset[^1].


[^1]: Smith, J.W., Everhart, J.E., Dickson, W.C., Knowler, W.C., & Johannes, R.S. (1988). Using the ADAP learning algorithm to forecast the onset of diabetes mellitus. In Proceedings of the Symposium on Computer Applications and Medical Care (pp. 261--265). IEEE Computer Society Press.